describe("Tickets", () => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

    it("fill all the text input fields", () => {
        const firstName = "Vitor Fernando";
        const lastName = "Oliveira";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("vitor@email.com");
        cy.get("#requests").type("Hue BR");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    it("select 2 tickets", () => {
        cy.get("#ticket-quantity").select("2");
    });

    it("select VIP", () => {
        cy.get("#vip").check();
    });

    it("select 'social media' checkbox", () => {
        cy.get("#social-media").check();
    });

    it("select 'friends' and 'publication', then uncheck 'friend'", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    })

    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    })

    it("alerts on invalid email", () => {
        cy.get("#email")
            .as("email")
            .type("djsfk.com");

        cy.get("#email.invalid")
            .as("invalidEmail")
            .should("exist")

        cy.get("@email")
            .clear()
            .type("emailvalido@email.com")

        cy.get("invalidEmail").should("not.exist");
    });

    it("fill and reset the form", () => {
        const firstName = "Vitor Fernando";
        const lastName = "Oliveira";
        const fullName = `${firstName} ${lastName}`;

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("vitor@email.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("Hue BR");
        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        );

        cy.get("#agree").click();
        cy.get("#signature").type(`${fullName}`);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");
    });

    it("fill mandatory fields using support command", () => {
        const customer = {
            firstName: "João",
            lastName: "Silva",
            email: "joaosilva@email.com"
        };

        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");
    });
});